#!/usr/bin/env python3

import os
import platform
import re
import subprocess
import sys
import time
import pwd

DSBUILD_SCRIPT_VERSION="0.2.0"

def set_env_if_not_set(key,fn):
    value=os.getenv(key)
    if not value:
        value=fn()
        if value:
            os.environ[key]=value
    
def shell(cmd,input=None):

    p=subprocess.Popen(cmd,
                    shell=True,
                    stdin=subprocess.PIPE,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.DEVNULL)
    if input:
        p.stdin.write(input)
    p.stdin.close() 
    output = p.stdout.read()
    p.wait(5)
    return output.decode("utf-8")

def shell_with_echo(cmd):
    if isinstance(cmd,list):
        print(" ".join(cmd))
    else:
        print(cmd)
    return shell(cmd)


def shell_with_echo_and_stream(cmd):
    if isinstance(cmd,list):
        print(" ".join(cmd))
    else:
        print(cmd)

    p=subprocess.Popen(cmd,
                    shell=(cmd[0] not in ["go","docker"]),
                    stdout=sys.stdout,
                    stderr=sys.stderr,
                    text=True)
    
    return p.wait(15)

def env(key):
    value=os.getenv(key)
    if value is None:
        value=""
    return value

def generate_build_version():
    version=""
    tag=shell("git tag --points-at HEAD").strip()
    commit=shell("git rev-parse --short HEAD").strip()
    changes=shell("git status -s")
    if (commit or tag) and not changes :
        if tag:
            version=tag
            m=re.match(r"[vV](\d.*)",version) # strip any v prefix before a number
            if m:
                version=m.group(1)
        else:
            version="0x"+env("DS_GIT_COMMIT")+"-commit"
    else:
        version=time.strftime("%Y%m%d-%H%M")+"-modified"

    branchline=shell("git branch")
    if not branchline:
        exit("No branch found")
    branch=branchline.split()[-1]
    if branch not in ["master","main"]:
        version+="-"+branch
    return version

def find_files(filename_glob,content_regex):
    files=shell("find . -iname '%s'"%(filename_glob)).splitlines()
    for file in files:
        file=file.strip()
        if content_regex:
            match=shell("grep -lEr '%s' '%s'"%(content_regex,file))    
            if match:
                yield file
        else:
            yield file

def load_default_env():
    if os.path.exists("./build.env"):
        for line in open("./build.env"):
            parts=line.strip().split("=",1)
            set_env_if_not_set(parts[0],lambda : parts[1])    

def create_build_env():
    set_env_if_not_set("DS_BUILD_AT",lambda : time.strftime("%a %d %b %Y %H:%M %Z").replace(" 0"," "))
    set_env_if_not_set("DS_BUILD_BY",lambda : os.getenv("CI_JOB_URL") or "%s@%s"%(pwd.getpwuid(os.getuid())[0],platform.node()))
    set_env_if_not_set("DS_BUILD_VERSION",lambda : generate_build_version())
    set_env_if_not_set("DS_BUILD_ARCH",lambda : platform.machine())

def exit(message):
    message=message.strip()
    if message:
        sys.stderr.write("Error: "+message+"\n")
        sys.exit(1)
    else:
        sys.exit(0)

#--------------------------------------
# assorted do_xxx_command

def do_help_command(args):
    print("Commands are...")
    print(" build.py env")
    print(" build.py go [vet|tidy|mod] .....")
    print(" build.py go [build] .....")     
    print(" build.py docker build .....")
    print(" build.py requirements")
    print(" build.py upgrade script")
    print(" build.py version")
    return 0

def do_env_command(args):
    #print("Environment...")
    for key,value in sorted(os.environ.items()):
        if key.startswith("DS_") or key.startswith("GO"):
            print("export %s='%s';"%(key,value))
    return 0

def do_upgrade_script():
    #myself=shell_with_echo("git config --get remote.origin.url").rstrip()
    #if myself.endswith("/dsbuild.git"):
    #    exit("Cannot update master version of build.py")
   
    newContent=shell("curl -s https://gitlab.com/dolbyn69/golib/dsbuild/-/raw/main/build.py?ref_type=heads")
    output=shell("python3 - version",newContent.encode("utf-8"))
    firstline=output.splitlines()[0]
    if firstline==DSBUILD_SCRIPT_VERSION:
        print("Already at latest version %s"%(DSBUILD_SCRIPT_VERSION))
        os.exit(0)

    upstreamVersion=firstline.split(".")
    upstreamVersion=DSBUILD_SCRIPT_VERSION.split(".")
    thisVersion=DSBUILD_SCRIPT_VERSION.split(".")

    if len(upstreamVersion)!=3 or len(thisVersion)!=3 or upstreamVersion[0] not in "0123456789":
        exit("Cannot replace version '%s' with '%s'"%(thisVersion,upstreamVersion))

    for i in range(3):
        if int(upstreamVersion[i])>int(thisVersion[i]):
            break
        elif int(upstreamVersion[i])<int(thisVersion[i]):
            exit("Cannot downgrade '%s' to '%s'"%(thisVersion,upstreamVersion))
        else:
            continue
    
    with open("build.py.new","w") as f:
        f.write(newContent) 
    os.chmod("build.py.new",0o755)
    os.rename("build.py.new","build.py")

    return 0

def do_upgrade_dependencies():
    shell_with_echo_and_stream("go get -u ./...")
    shell_with_echo_and_stream("go mod tidy")

def do_upgrade_command(args):
    if len(args)==0:
        exit("No sub command given")
    if args[0]=="script":
        do_upgrade_script()
    elif args[0]=="dependencies":
        do_upgrade_dependencies()
    else:
        exit("Unknown sub command '%s'"%(args[0]))


def do_git_command(args):
    gitvalues={}
    for line in shell("git config --list").splitlines():
        parts=line.strip().split("=",1)
        gitvalues[parts[0]]=parts[1]
    if "config" in args:
        set_env_if_not_set("DS_GITLAB_ACCESS_TOKEN",lambda : env("CI_JOB_TOKEN"))
        set_env_if_not_set("DS_GITLAB_BASE",lambda : "gitlab.com/")
        set_env_if_not_set("DS_GITLAB_USER_NAME",lambda : "gitlab-ci-token")
        if env("DS_GITLAB_ACCESS_TOKEN"):
            key='url."https://%s:%s@%s".insteadOf'%(env("DS_GITLAB_USER_NAME"),env("DS_GITLAB_ACCESS_TOKEN"),env("DS_GITLAB_BASE"))
            value='"https://%s"'%(env("DS_GITLAB_USER_NAME"),env("DS_GITLAB_ACCESS_TOKEN"),env("DS_GITLAB_BASE"))

            found=False
            for key,current in gitvalues.items():
                if current==value:
                    found=True 
            if not found:
                shell_with_echo('git config --worktree %s %s'%(key,value))

        if not gitvalues.get("user.name",None) and env("DS_GIT_USER_NAME"):
            shell_with_echo('git config --worktree %s %s',"user.name",env("DS_GIT_USER_NAME"))
        if not gitvalues.get("user.email",None) and env("DS_GIT_USER_EMAIL"):
            shell_with_echo('git config --worktree %s %s',"user.name",env("DS_GIT_USER_EMAIL"))
    shell_with_echo_and_stream('git config --list')
    tag=shell_with_echo("git tag --points-at HEAD")
    if tag=="":
        tag="<none>"
    print("   "+tag)
    shell_with_echo_and_stream("git rev-parse --short HEAD")
    shell_with_echo_and_stream("git status -s")
    branchLine=shell_with_echo("git branch")
    if branchLine=="":
        exit("No branch found")
    branch=branchLine.split()[-1]
    print("   "+branch)
    return 0

def do_requirements_command(args):

    if shell("alpine -v"):
        # apk.requirements: 
        if os.path.exists("apk.requirements.txt"):
            shell_with_echo_and_stream("cat apk.requirements.txt | xargs -r -t apk --no-cache add")
    else:
        # TODO consider apt.requirements
        # apt.requirements: 
        if os.path.exists("apt.requirements.txt"):
            shell_with_echo_and_stream("apt update")
            shell_with_echo_and_stream("DEBIAN_FRONTEND=noninteractive apt -y upgrade")
            shell_with_echo_and_stream("cat apt.requirements.txt | DEBIAN_FRONTEND=noninteractive xargs -r -t apt -y install")

    # pip.requirements: 
    if os.path.exists("pip.requirements"):
        shell_with_echo_and_stream("pip install --requirement pip.requirements.txt")

    return 0

def do_version_command(args):
    print(DSBUILD_SCRIPT_VERSION)

def do_go_command(args):
    for key,value in sorted(os.environ.items()):
        if key.startswith("GO"):
            print("export %s='%s';"%(key,value))
    #
    if len(args)==0:
        shell_with_echo_and_stream("go help")
        
    if args[0] in ["mod","version","work"]:
        # root scope 
        cmd=["go"]
        cmd.extend(args)
        return shell_with_echo_and_stream(cmd)
    else:
        #TODO for each pkg/cmd but not vendor
        if args[0]=="build":

            os.makedirs("bin",exist_ok=True)

            targets=[x for x in find_files("*.go",r"^package\s+main") if x.find("/vendor/")==-1 ] 
            targets=list(set([os.path.dirname(target) for target in targets]))

            for i in range(1,len(args)):
                print(args[i],targets)
                if args[i] in targets:
                    targets=[args[i]]
                    args=args[:i]+args[i+1:]
                    print("Building only",targets[0])
                    print("args",args)  
                    break
            
            ldflags="-X 'main.Version=%s' -X 'main.BuiltBy=%s' -X 'main.BuiltAt=%s' "%(env("DS_BUILD_VERSION"),env("DS_BUILD_BY"),env("DS_BUILD_AT"))

            for target in targets:
                target=target.strip()
                cmd=["go"]
                cmd.extend(args)
                if "-o" not in args:
                    cmd.extend(["-o","./bin/"+os.path.basename(target)])
                cmd.extend(["--ldflags",ldflags])
                cmd.append(target)
                shell_with_echo_and_stream(cmd)
        else:

            targets=shell("go list ./...| grep -v /vendor/").splitlines()
            for target in targets:
                target=target.strip()
                cmd=["go"]
                cmd.extend(args)
                cmd.append(target)
                shell_with_echo_and_stream(cmd)

        return 0

def do_docker_command(args):

    if len(args)==0:
        shell_with_echo_and_stream("go help")
        exit("No verb given for docker")
        
    extraargs=[]
    if args[0] in ["build","push"]:
        # make --build-arg 
        for varname in ["DS_BUILD_ARCH","DS_BUILD_BY","DS_BUILD_VERSION","DS_GITLAB_ACCESS_TOKEN","DS_GITLAB_USER_NAME","DS_GITLAB_BASE"]:      
            extraargs.extend(["--build-arg","%s=%s"%(varname,env(varname))])

        docker_files=list(find_files("*.Dockerfile",""))
        cwd=os.getcwd()
        label=env("DS_BUILD_VERSION")
        for docker_file in docker_files:
            docker_file=docker_file.strip()
            dirname,basename=os.path.split(docker_file)
            imagename=basename.replace(".Dockerfile","")
            print("cd",dirname)
            os.chdir(dirname)
            if args[0]=="push":
                shell_with_echo_and_stream("docker push %s:%s"%(imagename,label))
            elif args[0]=="build":
                shell_with_echo_and_stream("docker build . -f %s %s -t %s:%s"%(basename," ".join(extraargs),imagename,label))
            os.chdir(cwd)

    
    else:
        exit("build.py only supports the 'build' verb for docker")

    return 0

def do_package_command(args):
    do_env_command([])

    scripts=shell("find . -name package.sh").splitlines()
    for script in scripts:
        script=os.path.realpath(script.rstrip())
        dirname,basename=os.path.split(script)
        print("cd",dirname)
        os.chdir(dirname)
        r=shell_with_echo_and_stream("./"+basename)
        if r:
            return r

    return 0

#---------------------

def main(args):
    load_default_env()
    create_build_env()

    commands={
        "git":do_git_command,
        "help":do_help_command,
        "go":do_go_command,
        "version":do_version_command,
        "docker":do_docker_command,
        "env":do_env_command,
        "requirements":do_requirements_command,
        "package":do_package_command,
        "upgrade":do_upgrade_command
    }

    if len(args)==0:
        sys.stderr.write("No command given\n")
        do_help_command([])
        sys.exit(1)
    cmd=args[0].lower()

    cmd=commands.get(args[0].lower(),None)
    if cmd==None:
        cmd=do_help_command

    return cmd(args[1:])

if __name__ == "__main__":
    r=main(sys.argv[1:])
    sys.exit(0)

    